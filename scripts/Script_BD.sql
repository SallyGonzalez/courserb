--CREATE DATABASE BD_Course
--DROP DATABASE BD_Course
USE BD_Course
IF EXISTS(SELECT * FROM sys.objects WHERE object_id =  OBJECT_ID (N'dbo.Course') AND type in (N'U'))
DROP TABLE Course
GO
CREATE TABLE Course
(
	id INT IDENTITY(1,1),
	etiqueta_course VARCHAR(50)
);
GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'dbo.Student') AND type in (N'U'))
DROP TABLE Student
GO
CREATE TABLE Student
(
	id INT IDENTITY(1,1),
	student_name VARCHAR(100),
	gender VARCHAR(60),
	id_course INT,
	coverage VARCHAR(60),
	etiqueta_gender VARCHAR(50),
	comentarios VARCHAR(MAX)
);
GO
