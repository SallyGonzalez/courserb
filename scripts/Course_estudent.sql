use
BD_Course
GO

ALTER PROCEDURE Course_estudent
@table_student UDT_student READONLY,
@etiqueta_opcion VARCHAR(30)
AS
BEGIN
	----DECLARANDO TABLA DE MENSAJES ---
	DECLARE @TABLAMensajes TABLE
	(
		Resultado VARCHAR(MAX),
		Tipo VARCHAR(20),
		Mensaje VARCHAR(MAX),
		id INT
	);
	DECLARE @TABLA_STUDENTS UDT_student;
	DECLARE 
	@id INT = 0,
	 @student_name VARCHAR(100) = '',
	 @gender VARCHAR(60) = '',
	 @id_course INT = '',
	@coverage VARCHAR(60) = '',
	@etiqueta_gender VARCHAR(50) = '',
	@comentarios VARCHAR(MAX) = '';

	BEGIN TRY 
		BEGIN TRAN ADMINISTRAR_STUDENT
		IF(@etiqueta_opcion = 'ENVIAR')
		BEGIN
				INSERT INTO @TABLA_STUDENTS(id, student_name, gender, id_course, coverage,etiqueta_gender,comentarios)
				SELECT 
				ROW_NUMBER() OVER(ORDER BY id) as id, 
				student_name,
				gender,
				id_course,			
				coverage,
				etiqueta_gender,
				comentarios				
				FROM @table_student

				SET @id = (SELECT MIN(id) FROM @TABLA_STUDENTS);
				WHILE(@id IS NOT NULL)
				BEGIN
					SELECT 
					 @student_name = student_name,
					 @gender = gender,
					 @id_course = id_course,
					@coverage = coverage,
					@etiqueta_gender = etiqueta_gender,
					@comentarios = comentarios
					FROM @TABLA_STUDENTS WHERE id  = @id

					INSERT INTO dbo.Student
					(
						student_name,
						gender,
						id_course,
						coverage,
						etiqueta_gender,
						comentarios
					)
				OUTPUT  'OK','INFO',  'Registrado correctamente', inserted.id INTO @TABLAMensajes						
					VALUES
					(
						@student_name,
						@gender,
						@id_course,
						@coverage,
						@etiqueta_gender,
						@comentarios
					)
					SET @id = (SELECT MIN(id) FROM @TABLA_STUDENTS WHERE id > @id); 
					
				END

		END
		COMMIT TRAN ADMINISTRAR_STUDENT
	END TRY    
    BEGIN CATCH
		ROLLBACK TRAN ADMINISTRAR_STUDENT
	    			INSERT INTO @TABLAMensajes(Resultado,Tipo,Mensaje,id)VALUES('ERROR','ERROR',ERROR_MESSAGE(),0);
    END CATCH
	SELECT * FROM 	@TABLAMensajes
END