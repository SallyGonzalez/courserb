﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace coursewareDAO
{
    public class Connection
    {
        private SqlConnection conn;

        public SqlConnection CreateConnection()
        {
            Settings.SQLConn();
            return new SqlConnection(Settings.SQL_CONN_STRING);
        }
        public SqlConnection OpenConnection()
        {
          
            conn = CreateConnection();
            if (conn.State != ConnectionState.Open)
                conn.Open();
                return conn;
            
        }
        public bool CloseConnection()
        {
            if (conn.State != ConnectionState.Closed)
            {
                conn.Close();
                conn.Dispose();

            }
            return true;
        }

    }
}
