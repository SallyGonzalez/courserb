﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coursewareDAO
{
    public abstract class SQLHelper
    {
        // Tabla Hash para almacenar los par�metros en cach�.
        private static Hashtable parmCache = Hashtable.Synchronized(new Hashtable());

        /// <summary>
        /// Crea y ejecuta un comando para devolver un DataReader despu�s de recibir un par�metro.
        /// </summary>
        /// <param name="conn"> Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="singleParm">El objeto SqlParameter �nica para unirse a la consulta.</param>
        public static SqlDataReader ExecuteReaderSingleParm(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter singleParm)
        {
            SqlCommand cmd = new SqlCommand();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            cmd.Parameters.Add(singleParm);
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult);
            return rdr;
        }

        /// <summary>
        /// Crea y ejecuta un comando para devolver un DataReader de una sola fila, despu�s de recibir un par�metro.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="singleParm">El objeto SqlParameter �nica para unirse a la consulta.</param>
        public static SqlDataReader ExecuteReaderSingleRowSingleParm(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter singleParm)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            cmd.Parameters.Add(singleParm);
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);
            return rdr;
        }

        /// <summary>
        /// Crear y ejecutar un comando para devolver un DataReader sola fila despu�s de unirse a varios par�metros.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="cmdParms">Un conjunto de objetos SqlParameter para unirse a la consulta.</param>
        public static SqlDataReader ExecuteReaderSingleRow(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            PrepareCommand(cmd, cmdParms);
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow);
            return rdr;
        }

        /// <summary>
        /// Crear y ejecutar un comando para devolver un DataReader, hay par�metros que se utilizan en el comando.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        public static SqlDataReader ExecuteReaderNoParm(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult);
            return rdr;
        }

        /// <summary>
        /// Crear y ejecutar un comando para devolver un DataReader despu�s de unirse a varios par�metros.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL reales o el ServidorAplicaciones del Procedure almacenado dependiendo del Tipo de comando</param>
        /// <param name="cmdParms">Un conjunto de objetos SqlParameter para unirse a la consulta.</param>
        public static SqlDataReader ExecuteReader(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            PrepareCommand(cmd, cmdParms);
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult);
            return rdr;
        }

        /// <summary>
        /// Crear y ejecutar un comando para devolver un solo escalar (int) Valor despu�s de unirse a varios par�metros.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="cmdParms">Un conjunto de objetos SqlParameter para unirse a la consulta.</param>
        public static int ExecuteScalar(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            //  cmd.CommandType = cmdType;
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            PrepareCommand(cmd, cmdParms);
            int val = Convert.ToInt32(cmd.ExecuteScalar());
            return val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="cmdParms">Un conjunto de objetos SqlParameter para unirse a la consulta.</param>

        public static int ExecuteScalarProc(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            cmd.CommandType = cmdType;
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            PrepareCommand(cmd, cmdParms);

            SqlParameter returnValue = new SqlParameter("@Return_Value", DbType.Int32);
            returnValue.Direction = ParameterDirection.ReturnValue;

            cmd.Parameters.Add(returnValue);

            cmd.ExecuteNonQuery();
            int val = Int32.Parse(cmd.Parameters["@Return_Value"].Value.ToString());

            //   int val = Convert.ToInt32(cmd.ExecuteScalar());
            return val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="cmdParms">Un conjunto de objetos SqlParameter para unirse a la consulta.</param>

        public static SqlDataReader ExecuteReaderProc(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 6000;
            cmd.CommandText = cmdText;
            cmd.CommandType = cmdType;
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            PrepareCommand(cmd, cmdParms);

            SqlDataReader rdr = cmd.ExecuteReader();

            return rdr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="cmdParms">Un conjunto de objetos SqlParameter para unirse a la consulta.</param>

        public static SqlDataReader ExecuteReaderSingleParmProc(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter singleParm)
        {
            SqlCommand cmd = new SqlCommand();
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            cmd.CommandType = cmdType;
            cmd.Parameters.Add(singleParm);
            SqlDataReader rdr = cmd.ExecuteReader();
            return rdr;
        }

        /// <summary>
        /// Crear y ejecutar un comando para devolver un solo escalar (int) Valor despu�s de unirse a un solo par�metro.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�</param>
        /// <param name="trans">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="singleParm">Un objeto SqlParameter para unirse a la consulta.</param>
        public static int ExecuteScalarSingleParm(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter singleParm)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.Parameters.Add(singleParm);
            int val = Convert.ToInt32(cmd.ExecuteScalar());
            return val;
        }

        /// <summary>
        /// Crear y ejecutar un comando para devolver un solo escalar (int) Valor. No hay par�metros estar�n sujetos a la orden.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="singleParm">Un objeto SqlParameter para unirse a la consulta.</param>
        public static object ExecuteScalarNoParm(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            object val = cmd.ExecuteScalar();
            return val;
        }

        /// <summary>
        /// Crear y ejecutar un comando que no devuelve ning�n conjunto de resultados despu�s de unirse a varios par�metros.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de </param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando </param>
        /// <param name="cmdParms">Un conjunto de objetos SqlParameter para unirse a la consulta.</param>
        public static int ExecuteNonQuery(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            PrepareCommand(cmd, cmdParms);
            int val = cmd.ExecuteNonQuery();
            return val;
        }

        /// <summary>
        /// Crear y ejecutar un comando que no devuelve ning�n conjunto de resultados despu�s de unirse a un solo par�metro.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="singleParam">Un objeto SqlParameter para unirse a la consulta.</param>
        public static int ExecuteNonQuerySingleParm(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter singleParam)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            cmd.Parameters.Add(singleParam);
            int val = cmd.ExecuteNonQuery();
            return val;
        }

        /// <summary>
        /// Crear y ejecutar un comando que no devuelve ning�n conjunto de resultados despu�s de unirse a un solo par�metro.
        /// </summary>
        /// <param name="conn">Conexi�n a ejecutar en. Si no est� abierto, que va a estar aqu�.</param>
        /// <param name="trans">Transacci�n de ADO. Si es null, no se adjunta a la orden</param>
        /// <param name="cmdType">Tipo de comando ADO, como el texto o los procedimientos de</param>
        /// <param name="cmdText">El SQL real o el nombre del procedimiento almacenado dependiendo del tipo de comando</param>
        /// <param name="singleParam">Un objeto SqlParameter para unirse a la consulta.</param>
        public static int ExecuteNonQueryNoParm(SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = 3600;
            int val = cmd.ExecuteNonQuery();
            return val;
        }

        /// <summary>
        /// a�adir matriz de par�metros en la memoria cach�
        /// </summary>
        /// <param name="cacheKey">"La clave para el par�metro de cach�</param>
        /// <param name="cmdParms">una serie de SqlParameters ser almacenado en cach�</param>
        public static void CacheParameters(string cacheKey, params SqlParameter[] cmdParms)
        {
            parmCache[cacheKey] = cmdParms;
        }

        /// <summary>
        /// Recuperar los par�metros almacenados en cach�
        /// </summary>
        /// <param name="cacheKey">clave utilizada para par�metros de b�squeda </param>
        /// <returns>Array SqlParameters cach�</returns>
        public static SqlParameter[] GetCacheParameters(string cacheKey)
        {
            SqlParameter[] cachedParms = (SqlParameter[])parmCache[cacheKey];

            if (cachedParms == null)
                return null;

            SqlParameter[] clonedParms = new SqlParameter[cachedParms.Length];

            for (int i = 0, j = cachedParms.Length; i < j; i++)
                clonedParms[i] = (SqlParameter)((ICloneable)cachedParms[i]).Clone();

            return clonedParms;
        }

        /// <summary>
        /// Prepare una orden para su ejecuci�n
        /// </summary>
        /// <param name="cmd">objeto SqlCommand</param>
        /// <param name="conn">objeto SqlConnection</param>
        /// <param name="trans">objeto SqlTransaction</param>
        /// <param name="cmdType">Cmd tipo, por ejemplo, procedimiento o texto almacenado</param>
        /// <param name="cmdText">El texto de comando, por ejemplo, Seleccionar * de Productos</param>
        /// <param name="cmdParms">SqlParameters a utilizar en el comando </param>
        private static void PrepareCommand(SqlCommand cmd, SqlParameter[] cmdParms)
        {
            if (cmdParms != null)
            {
                for (int i = 0; i < cmdParms.Length; i++)
                {
                    SqlParameter parm = (SqlParameter)cmdParms[i];
                    cmd.Parameters.Add(parm);
                }
            }
        }
    }
}

