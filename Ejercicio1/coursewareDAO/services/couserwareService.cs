﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coursewareDAO
{
    public sealed class couserwareService
    {

        public List<course> ObtenerCourse()
        {

            try
            {

                SqlParameter[] Parametros = new SqlParameter[] {
                    new SqlParameter("@idCourse",SqlDbType.Int),
                    new SqlParameter("@etiqueta_opcion",SqlDbType.VarChar,50)

                };

                Parametros[0].Value = 0;
                Parametros[1].Value = "OBTENER";
             

                try
                {
                    Connection _connection = new Connection();
                    SqlConnection conn = _connection.OpenConnection();
                    List<course> listaCourse = new List<course>();

                    SqlDataReader recu = SQLHelper.ExecuteReaderProc(conn,null,CommandType.StoredProcedure, "Obtener_course", Parametros);
                    while (recu.Read())
                    {
                        course curso = new course();
                        curso.Id = recu.GetInt32(0);
                        curso.Etiqueta = recu.GetString(1);
                        listaCourse.Add(curso);

                    }

                    recu.Close();
                    recu.Dispose();
                    conn.Close();
                    return listaCourse;


                }
                catch (Exception ex)
                {
                    return null;

                }


            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<ResultDataModel> AdministrarCourse(List<student> students,string etiqueta_opcion)
        {
            try
            {
                List<ResultDataModel> resultados = new List<ResultDataModel>();

                DataTable tablaStudent = new DataTable();
                tablaStudent.Columns.Add("id",typeof(int));
                tablaStudent.Columns.Add("name",typeof(string));
                tablaStudent.Columns.Add("gender",typeof(string));
                tablaStudent.Columns.Add("id_course",typeof(int));
                tablaStudent.Columns.Add("coverage",typeof(string));
                tablaStudent.Columns.Add("etiqueta_gender",typeof(string));
                tablaStudent.Columns.Add("suggestions",typeof(string));

                foreach (student  stude in students)
                {
                    DataRow filaStudent = tablaStudent.NewRow();
                    filaStudent["id"] = stude.Id;
                    filaStudent["name"] = stude.Name;
                    filaStudent["gender"] = stude.Gender;
                    filaStudent["id_course"] = stude.Id_course;
                    filaStudent["coverage"] = stude.Coverage;
                    filaStudent["etiqueta_gender"] = stude.Etiqueta_gender;
                    filaStudent["suggestions"] = stude.Suggestions;
                    tablaStudent.Rows.Add(filaStudent);
                }


                SqlParameter[] Parametros = new SqlParameter[]
                {
                    new SqlParameter("@table_student",SqlDbType.Structured),
                    new SqlParameter("@etiqueta_opcion",SqlDbType.VarChar,50)
                };

                Parametros[0].Value = tablaStudent;
                Parametros[1].Value = etiqueta_opcion;

                try
                {
                    Connection _connection = new Connection();
                    SqlConnection conn = _connection.OpenConnection();
                    SqlDataReader recu = SQLHelper.ExecuteReaderProc(conn, null, CommandType.StoredProcedure, "Course_estudent", Parametros);
                   

                    while (recu.Read())
                    {
                        ResultDataModel respuesta = new ResultDataModel();
                        respuesta.Resultado = recu.GetString(0);
                        respuesta.Tipo = recu.GetString(1);
                        respuesta.Mensaje = recu.GetString(2);
                        respuesta.Id = recu.GetInt32(3);
                        resultados.Add(respuesta);
                    }

                    recu.Close();
                    recu.Dispose();
                    conn.Close();
                    return resultados;

                }
                catch (Exception ex)
                {
                    return null;
                }

              
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
