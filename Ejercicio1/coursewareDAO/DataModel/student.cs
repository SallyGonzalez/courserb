﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coursewareDAO
{
    public class student
    {
        private int _id;
        private string _name;
        private string _gender;
        private int _id_course;
        private string _coverage;
        private string _etiqueta_gender;
        private string _suggestions;

        public student()
        {

        }
        public student(int id, string name, string gender, int id_course, string coverage, string etiqueta_gender, string suggestions)
        {
            this._id = id;
            this._name = name;
            this._gender = gender;
            this._id_course = id_course;
            this._coverage = coverage;
            this._etiqueta_gender = etiqueta_gender;
            this._suggestions = suggestions;

        }

        public int Id
        {
            get { return _id; }
            set { this._id = value; }
        }
        public string Name
        {
            get { return _name; }
            set {this._name = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { this._gender = value; }
        }
        public int Id_course
        {
            get { return _id_course; }
            set { this._id_course = value; }
        }
        public string Coverage
        {
            get { return this._coverage; }
            set { this._coverage = value; }
        }
        public string Etiqueta_gender
        {
            get { return this._etiqueta_gender; }
            set { this._etiqueta_gender = value; }
        }
        public string Suggestions
        {
            get { return this._suggestions; }
            set { this._suggestions = value; }
        }
    }
}
