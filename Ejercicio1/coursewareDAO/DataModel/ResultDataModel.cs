﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coursewareDAO
{
   public class ResultDataModel
    {
        private int _id;
        private string _tipo;
        private string _resultado;
        private string _mensaje;

        public ResultDataModel()
        {

        }
        public ResultDataModel(int id,string tipo,string resultado,string mensaje)
        {
            this._id = id;
            this._tipo = tipo;
            this._resultado = resultado;
            this._mensaje = mensaje;
        }
        public int Id
        {
            get { return this._id; }
            set { this._id = value; }
        }
        public string Tipo
        {
            get { return this._tipo; }
            set { this._tipo = value; }
        }
        public string Resultado
        {
            get { return this._resultado; }
            set { this._resultado = value; }
        }
        public string Mensaje
        {
            get { return this._mensaje; }
            set { this._mensaje = value; }
        }
    }
}
