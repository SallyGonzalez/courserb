﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coursewareDAO
{
   public class course
    {
        private int id;
        private string etiqueta_course;

        public course()
        {

        }
        public course(int id,string etiqueta_course)
        {
            this.id = id;
            this.etiqueta_course = etiqueta_course;

        }

        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }


        public string Etiqueta
        {
            get { return this.etiqueta_course; }
            set { this.etiqueta_course = value; }
        }

    }
}
