﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace courseware
{
   public class mensajes
    {
        private int id;
        private string tipo;
        private string resultado;
        private string mensaje;

        public mensajes()
        {

        }
        public mensajes(int id,string tipo,string resultado,string mensaje)
        {
            this.id = id;
            this.tipo = tipo;
            this.resultado = resultado;
            this.mensaje = mensaje;
        }
        public int Id
        {
            get { return this.id; }
            set { value = this.id; }
        }
        public string Tipo
        {
            get { return this.tipo; }
            set { value = this.tipo; }
        }
        public string Resultado
        {
            get { return this.resultado; }
            set { value = this.resultado; }
        }
        public string Mensaje
        {
            get { return this.mensaje; }
            set { value = this.mensaje; }
        }
    }
}
