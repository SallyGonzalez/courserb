﻿using coursewareDAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace courseware
{
    public partial class frmMensajes : Form
    {
        conexion conn;
        string con;
        private string name_complet;
        private string gender;
        private string etiqueta_gender;
        private int id_course;
        private string coverage;
        private string suggestions;
        couserwareService courseware;

        public frmMensajes()
        {
            InitializeComponent();
          
        }

        public void frmMensajes_Load(object sender, EventArgs e)
        {
           
            fill_combox();

        }

        public void fill_combox()
        {
            List<course> curso = new List<course>();
            courseware = new couserwareService();
            curso = courseware.ObtenerCourse();
            if ( curso.Count > 0)
            {
                cboCourse.DataSource = curso;
                cboCourse.ValueMember = "ID";
                cboCourse.DisplayMember = "ETIQUETA";
               
            }


         }

      

        public void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validar())
                {
                    return;
                }

                List<ResultDataModel> resul = new List<ResultDataModel>();
                List<student> listaEstuadinte = new List<student>();
                string nombre = txtName.Text;
                student stu = new student();
                stu.Id = 0;
                stu.Name = nombre;
                if (checkMale.Checked)
                {
                    stu.Gender = checkMale.Text;

                } if (checkFemale.Checked)
                {
                    stu.Gender = checkFemale.Text;

                } if (checkOtro.Checked)
                {
                    stu.Gender = checkOtro.Text;
                }
                stu.Etiqueta_gender = txtGender.Text;
                stu.Id_course = Convert.ToInt32(cboCourse.SelectedValue);
                if (rbExcelente.Checked)
                {
                    stu.Coverage = rbExcelente.Text;

                }
                if (rbGood.Checked)
                {
                    stu.Coverage = rbGood.Text;

                }
                if (rbAverage.Checked)
                {
                    stu.Coverage = rbAverage.Text;

                }
                if (rbPoor.Checked)
                {
                    stu.Coverage = rbPoor.Text;
                }
                stu.Suggestions = txtComentarios.Text;
                listaEstuadinte.Add(stu);
                courseware = new couserwareService();
                resul = courseware.AdministrarCourse(listaEstuadinte,"ENVIAR");

                if (resul != null)
                {
                    if (resul.Count > 0)
                    {
                        foreach (ResultDataModel item in resul)
                        {
                            if (item.Resultado == "OK")
                            {
                                clear();
                                frmRespuesta frmRespuesta = new frmRespuesta();
                                frmRespuesta.ShowDialog();
                            }
                        }
                        
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void clear()
        {
            txtName.Text = "";
            txtGender.Text = "Insert your gender";
            txtComentarios.Text = "Please write a comment";
            checkFemale.Checked = false;
            checkMale.Checked = false;
            checkOtro.Checked = false;
            rbExcelente.Checked = false;
            rbGood.Checked = false;
            rbAverage.Checked = false;
            rbPoor.Checked = false;
            
        }
        public bool validar()
        {
            try
            {
                if (txtName.Text.Trim() == "" || txtName.Text == null)
                {
                    txtName.Focus();
                    MessageBox.Show("Please enter name", "Name", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (checkFemale.Checked == false && checkMale.Checked == false && checkOtro.Checked == false)
                {
                    MessageBox.Show("Please select gender", "Gender", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (txtGender.Text.Trim() == "Insert your gender" || txtGender.Text == null )
                {
                    MessageBox.Show("Please enter gender","Descripción Gender",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    return false;

                }
                if (rbExcelente.Checked == false && rbGood.Checked == false && rbAverage.Checked == false && rbPoor.Checked == false)
                {
                    MessageBox.Show("Please select coverage", "Coverage", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (txtComentarios.Text.Trim() == "Please write a comment" || txtComentarios.Text == null)
                {
                    txtComentarios.Focus();
                    MessageBox.Show("Please enter suggestions","Suggestions",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
